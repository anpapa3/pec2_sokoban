using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static CommonConstants;

public static class CommonUtils
{
    public static float colDistance = 1f;

    public static void CheckNeighbours(Transform transform, BoxCollider2D ownCollider,ref int numNeightbours, ref Dictionary<Direction, BoxCollider2D> neighboursColliders)
    {
        numNeightbours = 0;
        foreach (System.Collections.Generic.KeyValuePair<Direction, Vector2> entry in DirectionVectors)
        {
            neighboursColliders.Remove(entry.Key);
            BoxCollider2D neighbour = getNeighbourInDirection(transform.position, entry.Key, ownCollider);
            if (neighbour != null)
            {

                neighboursColliders.Add(entry.Key, neighbour);
                numNeightbours++;
            }
        }
    }

    public static BoxCollider2D getNeighbourInDirection(Vector2 origin, Direction direction, BoxCollider2D ownCollider)
    {
        BoxCollider2D result = null;
        Vector2 dir;
        DirectionVectors.TryGetValue(direction, out dir);

        RaycastHit2D[] hits = Physics2D.RaycastAll(origin, dir, colDistance);
        foreach(RaycastHit2D hit in hits)
        {
            if (hit.collider is BoxCollider2D collider)
                if (collider != ownCollider)
                    result = collider;
        }

        return result;
    }

    public static bool HasNeighBourInDirection(Dictionary<Direction, BoxCollider2D> neighboursColliders, Direction dir)
    {
        BoxCollider2D neighbourInDirection;
        neighboursColliders.TryGetValue(dir, out neighbourInDirection);

        return neighbourInDirection != null ? true : false;
    }
    
    public static void Move(Transform t, Direction d)
    {
        Vector2 moveDir;
        DirectionVectors.TryGetValue(d, out moveDir);
        t.Translate(moveDir);
    }
}
