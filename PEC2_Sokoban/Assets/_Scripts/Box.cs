using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static CommonConstants;
using static CommonUtils;

public class Box : MonoBehaviour
{
    [Header("Neighbours")]
    [SerializeField]
    private int numNeightbours = 0;

    [Header("Completion")]
    [SerializeField]
    private bool onTarget = false;

    private Dictionary<Direction, BoxCollider2D> neighboursColliders = new Dictionary<Direction, BoxCollider2D>();
    private BoxCollider2D ownCollider;
    private SpriteRenderer boxRenderer;

    void Start()
    {
        ownCollider = GetComponent<BoxCollider2D>();
        boxRenderer = GetComponentInChildren<SpriteRenderer>();
        CheckNeighbours(transform, ownCollider, ref numNeightbours, ref neighboursColliders);
    }

    public bool HasNeighbourInMovingDirection(Direction dir)
    {
        CheckNeighbours(transform, ownCollider, ref numNeightbours, ref neighboursColliders);
        return HasNeighBourInDirection(neighboursColliders, dir);
    }

    public void MoveBox(Direction dir)
    {
        Target target;
        onTarget = WillBeOnTarget(dir, out target);
        
        Move(transform, dir); 

        if (onTarget && target != null)
        {
            ChangeColor();
            target.PropagateTargetCompleted();
        }
        CheckNeighbours(transform, ownCollider, ref numNeightbours, ref neighboursColliders);
    }

    public bool WillBeOnTarget(Direction dir, out Target target)
    {
        target = null;

        BoxCollider2D neighbour;
        neighboursColliders.TryGetValue(dir, out neighbour);

        if(neighbour != null)
            target = neighbour.gameObject.GetComponent<Target>();

        return target != null;
    }

    public bool IsOnTarget()
    {
        return onTarget;
    }

    private void ChangeColor()
    {
        Color color = boxRenderer.color;
        color.r /= 1.5f;
        color.g /= 1.5f;
        color.b /= 1.5f;

        boxRenderer.color = color;
    }
}
