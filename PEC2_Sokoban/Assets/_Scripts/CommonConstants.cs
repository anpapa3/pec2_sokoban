using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public static class CommonConstants
{
    [SerializeField]
    [System.Serializable]
    public enum LevelElement { Floor = 0, Wall = 1, Player = 2, Box = 3, Target = 4, None = 5 }
    public enum Direction { Left = 2, Right = 3, Up = 1, Down = 0 }
    public enum EditActionsType { Add, Remove }
    public enum LevelSize { Small = 0, Medium = 1, Big = 2 , Null = 3}

    public enum GameState { MainMenu = 0, Game = 1}

    [SerializeField]
    [System.Serializable]
    public struct LevelElementObject
    {
        public int x, y;
        public LevelElement e;

        public LevelElementObject(int x, int y, LevelElement e)
        {
            this.x = x;
            this.y = y;
            this.e = e;
        }
    }
    public struct LevelSpecs {
        public int levelWidth;
        public int levelHeight;
        public int cameraSize;

        public LevelSpecs(int x, int y, int cam)
        {
            this.levelWidth = x;
            this.levelHeight = y;
            this.cameraSize = cam;
        }
    }

    public static LevelSpecs smallLevel = new LevelSpecs(12,6,8);
    public static LevelSpecs mediumLevel = new LevelSpecs(18, 10,10);
    public static LevelSpecs bigLevel = new LevelSpecs(24, 14,12);


    public static Dictionary<LevelSize, LevelSpecs> levelSizeMap = new Dictionary<LevelSize, LevelSpecs> {
        {LevelSize.Small, smallLevel},
        {LevelSize.Medium, mediumLevel},
        {LevelSize.Big, bigLevel}};

    public static Dictionary<Direction, Vector2> DirectionVectors = new Dictionary<Direction, Vector2> {
        {Direction.Up, Vector2.up},
        {Direction.Down, Vector2.down},
        {Direction.Left, Vector2.left},
        {Direction.Right, Vector2.right}};

    public static Dictionary<Direction, float> DirectionRotations = new Dictionary<Direction, float> {
        {Direction.Up, 180f},
        {Direction.Down, 0f},
        {Direction.Left, -90f},
        {Direction.Right, 90f}};
}
