using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using TMPro;

using static CommonConstants;
using UnityEditor;

public class LevelEditorController : MonoBehaviour
{
    public static LevelEditorController instance;

    [Header("Selected Prefab")]
    [SerializeField]
    private Image prefabImage;
    [SerializeField]
    private GameObject prefabSelected = null;
    [SerializeField]
    private GameObject prefabSelectedDisplay = null;

    [Header("Prefab Types")]
    [SerializeField]
    private GameObject floorPfb;
    [SerializeField]
    private GameObject wallPfb;
    [SerializeField]
    private GameObject playerPfb;
    [SerializeField]
    private GameObject boxPfb;
    [SerializeField]
    private GameObject targetPfb;

    [Header("Tiles")]
    [SerializeField]
    private GameObject tileMarkerPrefab;
    private GameObject[,] tilesMatrix; 
    private GameObject[,] objectMatrix;
    private GameObject[,] sceneryMatrix;

    private Dictionary<int, GameObject> prefabTypeMap = new Dictionary<int, GameObject>();

    [Header("Level Properties")]
    [SerializeField]
    private int levelSizeId = 0;
    [SerializeField]
    private int levelWidth = 16;
    [SerializeField]
    private int levelHeight = 10;
    [SerializeField]
    private int cameraSize = 8;

    [Header("UI")]
    [SerializeField]
    private Button saveButton;
    [SerializeField]
    private TextMeshProUGUI levelNameInput;
    [SerializeField]
    private GameObject levelsPanel;
    [SerializeField]
    private GameObject levelButtonPrefab;

    private GameObject level;
    private GameObject grid;

    private bool hasPlayerAlready = false;

    List<Level> levelsMap = new List<Level>();


    private void Awake()
    {
        if (instance == null)
            instance = this;
    }

    private void Start()
    {
        LoadLevels();
        CreateButtons();
        CreateMap();
        GenerateTiles();
        setSaveButton();
    }

    private void LoadLevels()
    {
        levelsMap.Clear();
        var levelsToErase = FindObjectsOfType<Level>();
        foreach(Level l in levelsToErase)
        {
            Resources.UnloadAsset(l);
        }
        Level[] levels = Resources.LoadAll<Level>("Levels");

        for (int i = 0; i < levels.Length; i++)
        {
            levelsMap.Add(levels[i]);
        }
    }

    private void CreateButtons()
    {
        for(int i = 0; i < levelsPanel.transform.childCount; i++) 
            Destroy (levelsPanel.transform.GetChild(i).gameObject);

        foreach(Level level in levelsMap)
        {
            GameObject instanceButton = Instantiate(levelButtonPrefab, levelsPanel.transform);

            level.GetSpecs(out string name, out LevelSize lev);
            Button b = instanceButton.GetComponent<Button>();
            instanceButton.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = name;
            b.onClick.AddListener(delegate { LoadMap(level); });
        }
    }

    private void GenerateTiles()
    {
        LevelSpecs levelSize = levelSizeMap.GetValueOrDefault((LevelSize)levelSizeId);
        this.levelWidth = levelSize.levelWidth;
        this.levelHeight = levelSize.levelHeight;
        this.cameraSize = levelSize.cameraSize;
        Destroy(grid);
        Destroy(level);

        grid = new GameObject("Grid"); 
        level = new GameObject("Level");

        tilesMatrix = new GameObject[levelWidth,levelHeight]; 
        objectMatrix = new GameObject[levelWidth, levelHeight];
        sceneryMatrix = new GameObject[levelWidth, levelHeight];

        for (int i = 0; i < levelWidth; i++)
            for(int j = 0; j < levelHeight; j++)
            {
                var auxGameObject = Instantiate(tileMarkerPrefab, new Vector3(i,j,0), Quaternion.identity, grid.transform);
                tilesMatrix[i,j] = auxGameObject;
            }

        Camera.main.transform.position = new Vector3(Mathf.RoundToInt(levelWidth / 2), Mathf.RoundToInt(levelHeight / 2), -10);
        Camera.main.orthographicSize = cameraSize;
    }

    private void Update()
    {
        if (InputController.instance.isLeftClicking())
        {
            PlacePrefab();
        }
        else if (InputController.instance.isRightClicking())
        {
            RemovePrefab();
        }
    }
    private void LateUpdate()
    {
        RenderPreviewPrefabOverMouse();
    }

    private void RenderPreviewPrefabOverMouse()
    {

        GetTilePos(out int tileX, out int tileY);
        if (prefabSelectedDisplay != null) { 
            prefabSelectedDisplay.transform.position = new Vector3(tileX,tileY,0);
        }
    }

    private void PlacePrefab()
    {
        GetTilePos(out int tileX, out int tileY);
        if (!BetweenLimits(tileX, tileY))
            return;
        var typePrefab = getType(prefabSelected);
        switch (typePrefab)
        {
            case LevelElement.Floor:
            case LevelElement.Wall:
                Destroy(sceneryMatrix[tileX, tileY]);
                sceneryMatrix[tileX, tileY] = Instantiate(prefabSelected, new Vector3(tileX, tileY, 0), Quaternion.identity, level.transform);
                break;
            case LevelElement.Box:
            case LevelElement.Target:
                var type = getType(objectMatrix[tileX, tileY]);
                if (type == LevelElement.Player)
                    hasPlayerAlready = false;
                Destroy(objectMatrix[tileX, tileY]);
                objectMatrix[tileX, tileY] = Instantiate(prefabSelected, new Vector3(tileX, tileY, 0), Quaternion.identity, level.transform);

                break;
            case LevelElement.Player:
                if (!hasPlayerAlready)
                {
                    Destroy(objectMatrix[tileX, tileY]);
                    objectMatrix[tileX, tileY] = Instantiate(prefabSelected, new Vector3(tileX, tileY, 0), Quaternion.identity, level.transform);
                    hasPlayerAlready = true;
                }
                break;
        }
        
        setSaveButton();
    }

    private void RemovePrefab()
    {
        GetTilePos(out int tileX, out int tileY);
        if (!BetweenLimits(tileX, tileY))
            return;

        var typePrefab = getType(prefabSelected);
        switch (typePrefab)
        {
            case LevelElement.Floor:
            case LevelElement.Wall:
                Destroy(sceneryMatrix[tileX, tileY]);
                sceneryMatrix[tileX, tileY] = null; 
                break;
            case LevelElement.Box:
            case LevelElement.Target:
            case LevelElement.Player:
                var type = getType(objectMatrix[tileX, tileY]);
                if (type == LevelElement.Player)
                    hasPlayerAlready = false;
                Destroy(objectMatrix[tileX, tileY]);
                objectMatrix[tileX, tileY] = null;
                break;
        }
        setSaveButton();
    }

    private void CreateMap()
    {
        prefabTypeMap.Add(0, floorPfb);
        prefabTypeMap.Add(1, wallPfb);
        prefabTypeMap.Add(2, playerPfb);
        prefabTypeMap.Add(3, boxPfb);
        prefabTypeMap.Add(4, targetPfb);
    }

    public void ChangePrefab(int id)
    {
        var lastPrefabSelected = prefabSelected;
        prefabSelected = prefabTypeMap.GetValueOrDefault(id);

        GameObject prefabGFX = prefabSelected.transform.GetChild(0).gameObject;
        Sprite prefabSprite = prefabGFX.GetComponent<SpriteRenderer>().sprite;
        Material prefabMaterial = prefabGFX.GetComponent<SpriteRenderer>().sharedMaterial;
        Color prefabColor = prefabGFX.GetComponent<SpriteRenderer>().color;

        if (prefabSprite != null && prefabMaterial != null && prefabColor != null)
        {
            prefabImage.sprite = prefabSprite;
            prefabImage.material = prefabMaterial;
            prefabImage.color = prefabColor;
        }
        else
            prefabSelected = lastPrefabSelected;

        GetTilePos(out int tileX, out int tileY);
        Destroy(prefabSelectedDisplay);
        prefabSelectedDisplay = Instantiate(prefabSelected, new Vector3(tileX, tileY, 0), Quaternion.identity);

    }

    private void GetTilePos(out int tileX, out int tileY)
    {
        var mousePos = InputController.instance.GetMousePosition();
        Vector2 worldPos = Camera.main.ScreenToWorldPoint(mousePos);

        tileX = Mathf.RoundToInt(worldPos.x);
        tileY = Mathf.RoundToInt(worldPos.y);
    }

    private bool BetweenLimits(int x, int y)
    {
        bool betweenX = (x >= 0 && x < levelWidth);
        bool betweenY = (y >= 0 && y < levelHeight);

        return betweenX && betweenY;
    }

    public void ChangeLevelSize(int l)
    {
        LevelSize le = (LevelSize)l;
        if (le.Equals(LevelSize.Null))
        {
            levelSizeId++;
            if (levelSizeId > 2)
                levelSizeId = 0;
        }
        else
            levelSizeId = (int)l;

        GenerateTiles();
        hasPlayerAlready = false;
        setSaveButton();
    }

    public void LoadMap(Level l)
    {
        l.GetSpecs(out string name, out LevelSize size);
        ChangeLevelSize((int)size);
        GenerateTiles();

        List<LevelElementObject> elements = l.getElements();
        foreach(LevelElementObject le in elements)
        {
            var prefab = prefabTypeMap.GetValueOrDefault((int) le.e);
            switch (le.e)
            {
                case LevelElement.Floor:
                case LevelElement.Wall:
                    sceneryMatrix[le.x, le.y] = Instantiate(prefab, new Vector3(le.x, le.y, 0), Quaternion.identity, level.transform);
                    break;
                case LevelElement.Player:
                    objectMatrix[le.x, le.y] = Instantiate(prefab, new Vector3(le.x, le.y, 0), Quaternion.identity, level.transform);
                    hasPlayerAlready = true;
                    break;
                case LevelElement.Box:
                case LevelElement.Target:
                    objectMatrix[le.x, le.y] = Instantiate(prefab, new Vector3(le.x, le.y, 0), Quaternion.identity, level.transform);
                    break;
                default:
                    break;
            }
        }

        setSaveButton();
    }
    public void SaveMap()
    {
        string levelName = levelNameInput.text;

        Level l = ScriptableObject.CreateInstance<Level>();
        l.setSpecs(levelName,(LevelSize) levelSizeId);

        for (int i = 0; i < levelWidth; i++)
            for (int j = 0; j < levelHeight; j++)
            {
                if (sceneryMatrix[i, j] != null)
                {
                    GameObject prefab = sceneryMatrix[i, j];
                    l.addElement(i, j, getType(prefab));
                }

                if (objectMatrix[i, j] != null)
                {
                    GameObject prefab = objectMatrix[i, j];
                    l.addElement(i, j, getType(prefab));
                }
            }
        #if (UNITY_EDITOR)
                AssetDatabase.CreateAsset(l, "Assets/Resources/Levels/" + levelName + ".asset");
                AssetDatabase.SaveAssets();
        #endif
        LoadLevels();
        CreateButtons();
        GenerateTiles();
        setSaveButton();
    }


    private LevelElement getType(GameObject prefab)
    {
        if (prefab == null)
            return LevelElement.None;

        PlayerController pc = prefab.GetComponent<PlayerController>();
        Box b = prefab.GetComponent<Box>();
        Target t = prefab.GetComponent<Target>();
        BoxCollider2D bc = prefab.GetComponent<BoxCollider2D>();

        if (pc != null)
        {
            return LevelElement.Player;
        }
        else if (b != null)
        {
            return LevelElement.Box;
        }
        else if (t != null)
        {
            return LevelElement.Target;
        }
        else if (bc != null)
        {
            return LevelElement.Wall;
        }
        else
            return LevelElement.Floor;
    }

    private bool CheckSameBoxesAsTargets()
    {
        int numBoxes = 0;
        int numTargets = 0;

        for (int i = 0; i < levelWidth; i++)
            for (int j = 0; j < levelHeight; j++)
            {
                if (objectMatrix[i, j] != null)
                {
                    GameObject prefab = objectMatrix[i, j];
                    if (getType(prefab).Equals(LevelElement.Box))
                        numBoxes++;
                    else if (getType(prefab).Equals(LevelElement.Target))
                        numTargets++;
                }
            }

        bool enoughBox = numBoxes > 0;
        bool enoughTarget = numTargets > 0;

        return (numTargets == numBoxes) && enoughBox && enoughTarget;
    }

    private void setSaveButton()
    {
        saveButton.interactable = hasPlayerAlready && CheckSameBoxesAsTargets();
    }
}
