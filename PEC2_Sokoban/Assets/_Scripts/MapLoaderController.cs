using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

using static CommonConstants;

public class MapLoaderController : MonoBehaviour
{
    public static MapLoaderController instance;

    [Header("Prefab Types")]
    [SerializeField]
    private GameObject floorPfb;
    [SerializeField]
    private GameObject wallPfb;
    [SerializeField]
    private GameObject playerPfb;
    [SerializeField]
    private GameObject boxPfb;
    [SerializeField]
    private GameObject targetPfb;

    private int actualLevel = 1;
    private int nextLevel = 1;
    private int maxLevel = 10;

    private int levelWidth = 0;
    private int levelHeight = 0;
    private int cameraSize = 0;

    private GameObject mapLevel;

    Dictionary<int, Level> levelsMap = new Dictionary<int, Level>();

    private void Awake()
    {
        if (instance == null)
            instance = this;
    }

    private void Start()
    {
        mapLevel = new GameObject("Level");
        LoadLevels();
    }

    private void LoadLevels()
    {
        Level[] levels = Resources.LoadAll<Level>("Levels");
        levelsMap.Clear();

        for (int i = 0; i < levels.Length; i++)
        {
            levelsMap.Add(i+1, levels[i]);
        }

        maxLevel = levels.Length;
        nextLevel = 1;
    }
    private void RestartVariables()
    {
        actualLevel = 1;
        nextLevel = 1;
        maxLevel = 0;

        levelWidth = 0;
        levelHeight = 0;
        cameraSize = 0;
        LoadLevels();
    }

    public bool ReloadLevel(out int numTargets)
    {
        nextLevel--;
        return LoadMap(out numTargets);
    }

    public bool LoadMap(out int numTargets)
    {
        for(int i = 0; i < mapLevel.transform.childCount; i++)
        {
            Destroy(mapLevel.transform.GetChild(i).gameObject);
        }

        numTargets = 0;

        if (nextLevel > maxLevel)
        {
            RestartVariables();
            return false;
        }

        Level l = levelsMap.GetValueOrDefault(nextLevel);

        l.GetSpecs(out string levelName, out LevelSize levelSize);

        LevelSpecs levelSpecs = levelSizeMap.GetValueOrDefault((LevelSize)levelSize);
        levelWidth = levelSpecs.levelWidth;
        levelHeight = levelSpecs.levelHeight;
        cameraSize = levelSpecs.cameraSize;

        List<LevelElementObject> elements = l.getElements();

        foreach (LevelElementObject le in elements) { 
            GameObject prefab = getPrefabFromType(le);
            if (prefab != null)
            {
                if (prefab.GetComponent<Target>() != null)
                    numTargets++;
                Instantiate(prefab, new Vector3(le.x, le.y, 0), Quaternion.identity, mapLevel.transform);
            }
        }

        Camera.main.transform.position = new Vector3(Mathf.RoundToInt(levelWidth / 2), Mathf.RoundToInt(levelHeight / 2), -10);
        Camera.main.orthographicSize = cameraSize;

        actualLevel = nextLevel;
        nextLevel++;

        return true;
    }

    public void setActualLevel(int id)
    {
        this.nextLevel = id;
    }

    private GameObject getPrefabFromType(LevelElementObject le)
    {
        var e = le.e;
        GameObject prefab = null;   
        switch (e)
        {
            case LevelElement.Floor:
                prefab = floorPfb;
                break;
            case LevelElement.Wall:
                prefab = wallPfb;
                break;
            case LevelElement.Player:
                prefab = playerPfb;
                break;
            case LevelElement.Box:
                prefab = boxPfb;
                break;
            case LevelElement.Target:
                prefab = targetPfb;
                break;
            default:
                break;
        }

        return prefab;
    }
}
