using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

using static CommonConstants;

public class GameController : MonoBehaviour
{
    public static GameController instance;
    public GameState gameState = GameState.MainMenu;

    private int numMoves = 0;
    private int numPushs = 0;
    private float timer = 0;

    private int targetsInActualLevel = 0;
    private int targetsCompleted = 0;

    [Header("UI")]
    [SerializeField]
    private GameObject mainMenu;
    [SerializeField]
    private GameObject gameUI;
    [SerializeField]
    private TextMeshProUGUI timerUI;
    [SerializeField]
    private TextMeshProUGUI moveUI;
    [SerializeField]
    private TextMeshProUGUI pushUI;

    private void Awake()
    {
        if(instance == null)
            instance = this;    
    }

    private void Update()
    {
        if (gameState.Equals(GameState.Game)) { 
            timer += Time.deltaTime;
            timerUI.text = formatTime(timer);
        }
    }

    public void StartGame()
    {
        bool mapLoaded = MapLoaderController.instance.LoadMap(out int numTargets);
        if (mapLoaded) { 
            targetsInActualLevel = numTargets;
            targetsCompleted = 0;
            mainMenu.SetActive(false);
            gameUI.SetActive(true);
            gameState = GameState.Game;
            RestartGameVars();
        }
    }

    public void ReloadLevel()
    {
        bool mapLoaded = MapLoaderController.instance.ReloadLevel(out int numTargets);
        if (mapLoaded)
        {
            targetsInActualLevel = numTargets;
            targetsCompleted = 0;
            RestartGameVars();
        }
    }

    public void RestartGameVars()
    {
        timer = 0;
        timerUI.text = formatTime(timer);
        numMoves = 0;
        numPushs = 0;
        SetMovePushUI();
    }

    private string formatTime(float time)
    {
        var min = (int) time / 60;
        var sec = (int) time % 60;

        string minS = min + "";
        string secS = sec + "";

        while(secS.Length < 2)
            secS = "0" + secS;

        return minS + ":" + secS;
    }

    public void AddMove(bool isPush)
    {
        numMoves++;
        if (isPush)
            numPushs++;

        SetMovePushUI();
    }

    private void SetMovePushUI()
    {
        moveUI.text = numMoves + "";
        pushUI.text = numPushs + "";
    }

    public void TargetCompleted()
    {
        targetsCompleted++;
        CheckLevelCompleted();

    }

    private void CheckLevelCompleted()
    {
        if(targetsCompleted == targetsInActualLevel)
        {
            NextLevel();
        }
    }

    private void NextLevel()
    {
        bool mapLoaded = MapLoaderController.instance.LoadMap(out int numTargets);
        if (mapLoaded)
        {
            targetsInActualLevel = numTargets;
            targetsCompleted = 0;
            RestartGameVars();
        }
        else
        {

            targetsInActualLevel = 0;
            targetsCompleted = 0;
            RestartGameVars();

            mainMenu.SetActive(true);
            gameUI.SetActive(false);
            gameState = GameState.MainMenu;
        }
    }
}
