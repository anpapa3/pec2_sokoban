using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputController : MonoBehaviour
{
    public static InputController instance;

    public InputActions Input;

    private Vector2 mousePosition;

    private void Awake()
    {
        if(instance == null)
            instance = this;
    }

    private void Start()
    {
        Input = new InputActions();
        Input.Enable();
    }

    private void Update()
    {
        mousePosition = Mouse.current.position.ReadValue();
    }

    public bool isLeftClicking()
    {
        return Mouse.current.leftButton.IsPressed();
    }

    public bool isRightClicking()
    {
        return Mouse.current.rightButton.IsPressed();
    }

    public Vector2 GetMousePosition()
    {
        return mousePosition;
    }

}
