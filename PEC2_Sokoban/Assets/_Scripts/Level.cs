using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using static CommonConstants;

[SerializeField]
[System.Serializable]
public class Level : ScriptableObject
{
    [SerializeField]
    private string levelName;
    [SerializeField]
    private LevelSize levelSize;
    [SerializeField]
    private List<LevelElementObject> elements = new List<LevelElementObject>();

    public void addElement(int x, int y, LevelElement e)
    {
        elements.Add(new LevelElementObject(x, y, e));
    }

    public void setSpecs(string levelName, LevelSize levelSize)
    {
        this.levelName = levelName;
        this.levelSize = levelSize;
    }

    public void GetSpecs(out string level,out LevelSize levelS)
    {
        level = levelName;
        levelS = levelSize;
    }

    public List<LevelElementObject> getElements()
    {
        return elements;
    }
}
