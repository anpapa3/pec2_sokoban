using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static CommonConstants;
using static CommonUtils;

public class PlayerController : MonoBehaviour
{
    public static PlayerController instance;

    [Header("Direction & Neighbours")]
    [SerializeField]
    private Direction facingDirection = Direction.Down; 
    [SerializeField]
    private int numNeightbours = 0;

    private Dictionary<Direction, BoxCollider2D> neighboursColliders = new Dictionary<Direction, BoxCollider2D>();
    private SpriteRenderer playerGfx;
    private BoxCollider2D ownCollider;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        ownCollider = GetComponent<BoxCollider2D>();
        playerGfx = GetComponentInChildren<SpriteRenderer>();

        InputController.instance.Input.GameControls.MoveUp.started += context => instance.MovePlayer(Direction.Up);
        InputController.instance.Input.GameControls.MoveDown.started += context => instance.MovePlayer(Direction.Down);
        InputController.instance.Input.GameControls.MoveLeft.started += context => instance.MovePlayer(Direction.Left);
        InputController.instance.Input.GameControls.MoveRight.started += context => instance.MovePlayer(Direction.Right);
        InputController.instance.Input.GameControls.Pause.started += context => GameController.instance.ReloadLevel();
        CheckNeighbours(transform,ownCollider,ref numNeightbours,ref neighboursColliders);
    }

    
    public void Face()
    {
        playerGfx.transform.localEulerAngles = new Vector3(0, 0, DirectionRotations.GetValueOrDefault(facingDirection));
    }

    private void MovePlayer(Direction dir)
    {
        if (GameController.instance.gameState != GameState.Game)
            return;

        CheckNeighbours(transform, ownCollider, ref numNeightbours, ref neighboursColliders);

        if (!HasNeighBourInDirection(neighboursColliders, dir))
        {
            facingDirection = dir;
            Face();
            Move(transform, facingDirection);
            CheckNeighbours(transform, ownCollider, ref numNeightbours, ref neighboursColliders);
            GameController.instance.AddMove(false);
        }
        else
        {
            BoxCollider2D neighbour;
            neighboursColliders.TryGetValue(dir, out neighbour);

            Box box = neighbour.GetComponent<Box>();
            if(box != null)
            {
                Target t;
                if ((!box.HasNeighbourInMovingDirection(dir) && !box.IsOnTarget())
                    ||
                    (box.HasNeighbourInMovingDirection(dir) && box.WillBeOnTarget(dir, out t) && !box.IsOnTarget() ))
                {
                    facingDirection = dir;
                    Face();
                    box.MoveBox(facingDirection);
                    Move(transform, facingDirection);
                    CheckNeighbours(transform, ownCollider, ref numNeightbours, ref neighboursColliders);
                    GameController.instance.AddMove(true);
                }
            }
            CheckNeighbours(transform, ownCollider, ref numNeightbours, ref neighboursColliders);
        }
    }

    public void setFacingDirection(Direction dir)
    {
        this.facingDirection = dir;
    }
}
